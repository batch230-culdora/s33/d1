// console.log("HELLO world");

// >> Synchronous proof
// JavaScript is by default synchronous, meaning that it only excecutes one statement at a time
/*
console.log("Hello World pt. 2");
conosle.log("hellow again");
console.log("Good bye");*/
// Code blocking - wating for the specific statement to finish before executing the next statement
/*
for(let i=0; i<=1500; i++){
	console.log(i);
}
console.log("Hello again")
*/

// Asynchronous means that we can proceed to execute other statement, while time consuming code is running in the background

// The Fetch API that allows us to asynchronously request for a resource (data).
// "fetch()" method in javascript is used to request to the server and load information on the webpages
// Syntax:
	// fetch("apiURL")

// [SECTION] Getting all posts
fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => console.log(res.json()));

// The ".then()" method captures the response object and returns a promise which will be "fullfilled" or "rejected"
//  .json() identifies a network response and converts it directly to JavaScript object


fetch("https://jsonplaceholder.typicode.com/posts")
//.then(res => console.log(res));
.then(res => console.log(res.status));



fetch("https://jsonplaceholder.typicode.com/posts")
.then(res => res.json()) // Returns response that is converted into JavaScript object
.then(response => console.log(response)); // Display the fullfilled response from the previous .then



// Translating arrow function to traditional function
/*fetch("https://jsonplaceholder.typicode.com/posts")
.then(function(res){ 
	return res.json()
})
.then(function(response){ 
	return console.log(response)
})*/

// [SECTION] Getting a specific post
// ":id" is a wildcard where you can put any value it then creates a link between "id" parameter in the URL and value provided in the URL
/*fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(res => res.json())
.then(response => console.log(response));*/

// The async and await keyword to achieve asynchronous code.

async function fetchData(){
	let result = await(fetch("https://jsonplaceholder.typicode.com/posts"))
	console.log(result);
	let json = await result.json();
	console.log(json);
	console.log("Hello World")
}
fetchData();

//  GET, POST, PUT, PATCH, DELETE
// [GET] Getting a specific document 

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then(res => res.json())
.then(response => console.log(response));

// [POST] Inserting a document/field
/*
	Syntax:
		fetch("apiURL", {options})
		.then((response) => {})
		.then((response) => {})
*/

fetch("https://jsonplaceholder.typicode.com/posts", 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			title: "New Post",
			body: "Hello World",
			userId: 1
		})
	})
.then(response => response.json())
.then(json => console.log(json))

// [PUT] Updating a whole document (all fileds)

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "updated post",
		body: "Hello Again",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// [PATCH] Updating a specific field of a document
// PUT vs PATCH
	// PATCH is used to update a single/several properties
	// PUT is used to update the whole document/object

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post title"
		})
})
.then(response => response.json())
.then(json => console.log(json))

// [DELETE] Deleting a document
fetch("https://jsonplaceholder.typicode.com/posts/1", 
	{
	method: "DELETE"
	}
).then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todo => {
		return todo.title;
	}))

	console.log(list);

});

